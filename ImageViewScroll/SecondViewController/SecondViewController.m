//
//  SecondViewController.m
//  ImageViewScroll
//
//  Created by SI3 on 2018/06/20.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong,nonatomic) UIImage *image;
@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView.minimumZoomScale = 1;
    self.scrollView.maximumZoomScale = 3;
    self.scrollView.delegate = self;
    
    NSDictionary *a = [NSDictionary dictionary];
    a = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    NSData *test = [a objectForKey:@"myImg"];
    UIImage* image = [UIImage imageWithData:test];
    self.image = image;
    self.imageView.image = image;

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    
    return self.imageView;
}


- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)saveButtonEvent
{
    //保存する画像を指定
    
}



- (IBAction)ok:(id)sender {
    UIImage *srcImage = self.image;
    UIImage *dstImage = nil;

    CGSize imageViewSize = self.scrollView.frame.size;
    //CGSize imageViewSize = self.imageView.frame.size;
    
    CGPoint p = self.scrollView.contentOffset;

    
    CGRect clipRect = CGRectMake(p.x, p.y,imageViewSize.width,imageViewSize.height);
    
    NSLog(@"yoko(%f) tate(%f)",imageViewSize.width,imageViewSize.height);

//    // 指定された画像から指定された範囲の抜き出す
//    CGImageRef cliped = CGImageCreateWithImageInRect(srcImage.CGImage, clipRect);
//
//    // CGImageRefからUIImageを作る
//    dstImage = [UIImage imageWithCGImage:cliped
//                                   scale:[[UIScreen mainScreen] scale]
//                             orientation:UIImageOrientationUp];
//    CGImageRelease(cliped);
//
//    SEL selector = @selector(onCompleteCapture:didFinishSavingWithError:contextInfo:);
//
//    UIImageWriteToSavedPhotosAlbum(dstImage, self, selector, nil);
//
    
    
    
    
}


- (void)onCompleteCapture:(UIImage *)screenImage
 didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    NSString *message = @"画像を保存しました";
    if (error) message = @"画像の保存に失敗しました";
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                {
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
