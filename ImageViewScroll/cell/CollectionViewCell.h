//
//  CollectionViewCell.h
//  ImageViewScroll
//
//  Created by SI3 on 2018/06/13.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImgView;

@end
