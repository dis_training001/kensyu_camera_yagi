//
//  PickerViewController.m
//  ImageViewScroll
//
//  Created by SI3 on 2018/06/14.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import "PickerViewController.h"
#import "CollectionViewCell.h"
#import "ImageDataManager.h"

@interface PickerViewController ()<UIScrollViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) ImageDataManager *imageDataManager;

@property (weak, nonatomic) IBOutlet UICollectionView *imgCollectionView;
@property PHFetchResult *assetCollections;
@property PHFetchResult *assets;
@property PHAssetCollection *assetCollection;
@property UIImage * test;
@property int sizeChk;
@property int dateChk;


@end

@implementation PickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sizeChk = 0;
    self.dateChk = 0;
    self.assetCollections = [PHAssetCollection fetchAssetCollectionsWithType: PHAssetCollectionTypeSmartAlbum
                                                                               subtype: PHAssetCollectionSubtypeSmartAlbumUserLibrary
                                                                               options: nil];
    self.assetCollection = self.assetCollections.firstObject;
    self.assets = [PHAsset fetchAssetsInAssetCollection:self.assetCollection options: nil];
    
    

    
    
    self.imgCollectionView.delegate = self;
    self.imgCollectionView.dataSource = self;
    UINib *nib = [UINib nibWithNibName:@"CollectionViewCell" bundle:nil];
    [self.imgCollectionView registerNib:nib forCellWithReuseIdentifier:@"Cell"];
    

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.assets.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor redColor];
    
    //　self.assetsから画像取得するオブジェクト取得
    PHAsset* asset = self.assets[indexPath.row];
    
    // 画像取得
    [[PHImageManager defaultManager] requestImageForAsset:asset
                                               targetSize:CGSizeMake(300,300)
                                              contentMode:PHImageContentModeAspectFit
                                                  options:nil
                                            resultHandler:^(UIImage *result, NSDictionary *info) {
                                                if (result) {
                                                    // 画像設定
                                                    cell.thumbnailImgView.image = result;
                                                }
                                            }];

    
    return cell;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
  
    PHAsset* asset = self.assets[indexPath.row];
    
    
    [[PHImageManager defaultManager] requestImageForAsset:asset
                                               targetSize:CGSizeMake(600,600)
                                              contentMode:PHImageContentModeAspectFit
                                                  options:nil
                                            resultHandler:^(UIImage *result, NSDictionary *info) {
                                                if (result) {
                                                    self.test = result;

                                                    NSData *imageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(self.test,1)];
                                                    [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:@"myImg"];
                                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                                    NSDictionary *a = [NSDictionary dictionary];
                                                    a = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
                                                        NSLog(@"%@", result);
                                                }
                                            }];

    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)sendSet{
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100,100);
}


- (IBAction)sizeSortButton:(id)sender {
    self.dateChk = 0;
    if(self.sizeChk == 0){
    PHFetchOptions *fetchOptions = [PHFetchOptions new];
    fetchOptions.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"pixelWidth" ascending:YES], ];
    self.assets = [PHAsset fetchAssetsWithOptions:fetchOptions];
        self.sizeChk = 1;
    }else if (self.sizeChk == 1){
        PHFetchOptions *fetchOptions = [PHFetchOptions new];
        fetchOptions.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"pixelWidth" ascending:NO], ];
        self.assets = [PHAsset fetchAssetsWithOptions:fetchOptions];
        self.sizeChk = 0;
    }
    [self.imgCollectionView reloadData];
}


- (IBAction)dateSortButton:(id)sender {
    self.sizeChk = 0;
    if(self.dateChk == 0){
    PHFetchOptions *fetchOptions = [PHFetchOptions new];
    fetchOptions.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO], ];
    self.assets= [PHAsset fetchAssetsWithOptions:fetchOptions];
     [self.imgCollectionView reloadData];
        self.dateChk =1;
        
    }else if (self.dateChk == 1){
        PHFetchOptions *fetchOptions = [PHFetchOptions new];
        fetchOptions.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES], ];
        self.assets= [PHAsset fetchAssetsWithOptions:fetchOptions];
        [self.imgCollectionView reloadData];
        self.dateChk =0;
    }
    [self.imgCollectionView reloadData];
}
- (IBAction)dismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
