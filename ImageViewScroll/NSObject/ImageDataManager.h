//
//  ImageDataManager.h
//  ImageViewScroll
//
//  Created by SI3 on 2018/06/15.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ImageDataManagerDelegate <NSObject>

@end

@interface ImageDataManager : NSObject

+ (ImageDataManager *)ManageData;

@property (weak, nonatomic) id<ImageDataManagerDelegate> delegate;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSMutableArray *imgData;
@end
