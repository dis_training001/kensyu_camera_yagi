//
//  ViewController.m
//  ImageViewScroll
//
//  Created by SI3 on 2018/06/08.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import "ViewController.h"
#import "CollectionViewCell.h"
#import "PickerViewController.h"
#import "ImageDataManager.h"


@interface ViewController ()<UIScrollViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UICollectionViewDelegate,UIPickerViewDelegate>
@property (strong, nonatomic) ImageDataManager *imageDataManager;
@property (weak, nonatomic) IBOutlet UIImageView *testImageView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageVIew;
@property (weak, nonatomic) UIImage * trmingImg;
@property (strong, nonatomic) NSArray *imgArray;
@property (weak,nonatomic) UIImage *image;

- (IBAction)albumButton:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.imgArray = [NSArray array];
    
    //self.imageVIew.image = [UIImage imageNamed:@"test"];
    self.scrollView.minimumZoomScale = 1;
    self.scrollView.maximumZoomScale = 1.5;
    self.scrollView.delegate = self;


}




-(void)viewDidAppear:(BOOL)animated{
    [self.scrollView setZoomScale:1.0f];
    
    if(self.image ==nil){
        NSDictionary *a = [NSDictionary dictionary];
        a = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
        NSData *test = [a objectForKey:@"myImg"];
        UIImage* image = [UIImage imageWithData:test];
        self.image = image;
        self.imageVIew.image = image;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    
    return self.imageVIew;
}

- (IBAction)pushedCameraButton:(id)sender {
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    // カメラが利用可能かチェック
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        // インスタンスの作成
        UIImagePickerController *cameraPicker = [[UIImagePickerController alloc] init];
        cameraPicker.sourceType = sourceType;
        cameraPicker.delegate = self;
        
        [self presentViewController:cameraPicker animated:YES completion:nil];
    }else{
        NSLog(@"error");
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)imagePicker {
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)albumButton:(id)sender {
    self.image = nil;
    [self performSegueWithIdentifier:@"go" sender:self];
       // [self.navigationController pushViewController:pickerViewController animated:YES];
}




- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    PHPhotoLibrary *library = [PHPhotoLibrary sharedPhotoLibrary];
    [library performChanges:^{
        self.image = info[UIImagePickerControllerOriginalImage];
        [PHAssetChangeRequest creationRequestForAssetFromImage:self.image];
        dispatch_async(
                       dispatch_get_main_queue(),
                       ^{
                           (self.imageVIew).image = self.image;
                       }
                       );
    }
          completionHandler:^(BOOL success, NSError * _Nullable error) {
          }];
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)albumFromFolder:(id)sender {
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;

    
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        UIImagePickerController *cameraPicker = [[UIImagePickerController alloc] init];
        cameraPicker.sourceType = sourceType;
        cameraPicker.delegate = self;
        
        // ビューを開く
       // [self presentViewController:cameraPicker animated:YES completion:nil];
        [self.navigationController pushViewController:cameraPicker animated:YES];
    }
}

- (IBAction)test:(id)sender {
    [self performSegueWithIdentifier:@"gogo" sender:self];
//    float width = self.image.size.width;
//    float height = self.image.size.height;
//    if(width > 750 && height > 750){
//        
//    }else{
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"トリミングできる最小サイズ以下です。" message:nil preferredStyle:UIAlertControllerStyleAlert];
//        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//            [self dismissViewControllerAnimated:YES completion:nil];
//        }]];
//        [self presentViewController:alertController animated:YES completion:nil];
//    }


}




@end
